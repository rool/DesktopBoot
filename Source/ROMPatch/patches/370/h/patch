/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* patches/370/patch.h */

/* sum of patches for RISC OS 3.70 */

#include "patches/370/ROMcrc.h"
#include "patches/370/RTCAfix.h"
#include "patches/370/ADFSpatch.h"
#include "patches/370/PPRTpatch.h"
#include "patches/370/STMHpatch.h"
#include "patches/370/WMTBpatch.h"
#include "patches/370/CLIBpatch.h"

static patchlist_proc patchlist370[] =
{
  RTCAfix370_proc,
  ADFSpatches370_proc,
  PPRTpatches370_proc,
  STMHpatches370_proc,
  WMTBpatches370_proc,
  CLIBpatches370_proc,
  NULL
};

static ROMentry_t ROMentry370 =
{
  romcrc370,
  1,                     /* 3.70 supports ROM-space write protect */
  1,                     /* 3.70 checksum can be believed */
  4,                     /* 3.70 comes on 4MB of ROM */
  M_name370,
  patchlist370
};
